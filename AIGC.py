import json
import requests
import tkinter as tk
from tkinter import scrolledtext, ttk
import time

def get_access_token(api_key, api_secret):
    url = "https://chatglm.cn/chatglm/assistant-api/v1/get_token"
    data = {
        "api_key": api_key,
        "api_secret": api_secret
    }
    response = requests.post(url, json=data)
    response.raise_for_status()
    token_info = response.json()
    return token_info['result']['access_token']

def handle_response(data_dict):
    message = data_dict.get("message", {})
    content = message.get("content", {})
    response_type = content.get("type")

    if response_type:
        handlers = {
            "text": lambda c: c.get("text", "未提供文本"),
            "image": lambda c: ", ".join(image.get("image_url") for image in c.get("image", [])),
            "code": lambda c: c.get("code"),
            "execution_output": lambda c: c.get("content"),
            "system_error": lambda c: c.get("content"),
            "tool_calls": lambda d: d['tool_calls'],
            "browser_result": lambda c: f"浏览器结果 - 标题: {json.loads(c.get('content', '{}')).get('title')} URL: {json.loads(c.get('content', '{}')).get('url')}"
        }
        return handlers.get(response_type, lambda _: None)(content)
    else:
        return None

def send_message(assistant_id, access_token, prompt, conversation_id=None, file_list=None, meta_data=None):
    url = "https://chatglm.cn/chatglm/assistant-api/v1/stream"
    headers = {
        "Authorization": f"Bearer {access_token}",
        "Content-Type": "application/json"
    }

    data = {
        "assistant_id": assistant_id,
        "prompt": prompt,
    }

    if conversation_id:
        data["conversation_id"] = conversation_id
    if file_list:
        data["file_list"] = file_list
    if meta_data:
        data["meta_data"] = meta_data

    try:
        with requests.post(url, json=data, headers=headers, stream=True) as response:
            response.raise_for_status()
            complete_output = ""
            last_length = 0

            for line in response.iter_lines():
                if line:
                    decoded_line = line.decode('utf-8')
                    if decoded_line.startswith('data:'):
                        data_dict = json.loads(decoded_line[5:])
                        output = handle_response(data_dict)

                        if output:
                            new_text = output[last_length:]
                            complete_output += new_text
                            last_length = len(output)
                            yield new_text
    except requests.RequestException as e:
        print(f"请求失败: {e}")
        return None

class ChatApp:
    def __init__(self, master):
        self.master = master
        master.title("情感交流AIGC Chat")
        master.geometry("720x660")
        master.resizable(True, True)

        # 创建聊天框架
        self.chat_frame = tk.Frame(master, bg="#e6f7ff", bd=2, relief=tk.RAISED)
        self.chat_frame.pack(padx=10, pady=10, fill=tk.BOTH, expand=True)

        # 创建聊天区域
        self.text_area = scrolledtext.ScrolledText(self.chat_frame, state='disabled', wrap=tk.WORD, height=20, width=60, bg="#f0f0f0", fg="#333", font=('Helvetica', 12))
        self.text_area.pack(padx=5, pady=5, fill=tk.BOTH, expand=True)

        # 创建输入框架
        self.input_frame = tk.Frame(master, bg="#ffffff", bd=2, relief=tk.RAISED)
        self.input_frame.pack(padx=10, pady=(0, 10), fill=tk.X)

        # 创建输入区域
        self.entry = tk.Entry(self.input_frame, width=70, bg="#fff", fg="#000", font=('Helvetica', 12))
        self.entry.pack(side=tk.LEFT, padx=5, pady=5, fill=tk.X, expand=True)

        # 创建发送按钮
        self.send_button = ttk.Button(self.input_frame, text="发送", command=self.send_message)
        self.send_button.pack(side=tk.RIGHT, padx=5)

        # API凭据
        self.api_key = '33ca3b2a9eae16e3'  # 替换为你的API密钥
        self.api_secret = 'f8f7d66d348286ce47f5fbb47d540b5c'  # 替换为你的API秘密
        self.assistant_id = "66ed1c5f382a88e15b169638"  # 替换为你的助手ID
        self.token = get_access_token(self.api_key, self.api_secret)

    def send_message(self):
        user_input = self.entry.get()
        self.entry.delete(0, tk.END)

        if user_input.lower() == "s":
            self.text_area.config(state='normal')
            self.text_area.insert(tk.END, "\n结束对话。\n")
            self.text_area.config(state='disabled')
            return

        self.text_area.config(state='normal')
        self.text_area.insert(tk.END, f"\n用户: {user_input}\n\n")
        self.text_area.config(state='disabled')

        response_generator = send_message(self.assistant_id, self.token, user_input)
        self.output_response(response_generator)

    def output_response(self, response_generator):
        self.master.after(100, self.update_response, response_generator)

    def update_response(self, response_generator):
        try:
            response = next(response_generator)
            self.text_area.config(state='normal')
            for char in response:
                self.text_area.insert(tk.END, char)
                self.text_area.see(tk.END)
                self.text_area.update()
                time.sleep(0.05)  # 加快字符输出速度
            self.text_area.config(state='disabled')
            self.text_area.insert(tk.END, "\n\n", 'ai')  # Add newline after response
            self.master.after(100, self.update_response, response_generator)

        except StopIteration:
            pass

if __name__ == "__main__":
    root = tk.Tk()
    app = ChatApp(root)
    root.mainloop()
